#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR=$(dirname "$0")
CURRDIR=$(realpath "$CURRDIR/..")

REPODIR=$(realpath "$CURRDIR/..")
TODAY=$(date +%Y-%m-%d)

# Expanding incomplete values

echo "Processing incomplete values"
set +e
FILENAMES=$(grep -r -l "null|integer|float|string|object|array" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata --exclude-dir=placeholder-phpunit --exclude=.php-cs-fixer.dist.php)
set -e

for FILENAME in $FILENAMES
do
	echo "Processing $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i 's/null|integer|float|string|object|array/null|boolean|integer|float|string|object|array/g' "$FILENAME"
done


# Expanding 'mixed[] values with array

echo "Processing mixed[] values"
set +e
FILENAMES=$(grep -r -l "mixed\[\]" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata --exclude-dir=placeholder-phpunit --exclude=.php-cs-fixer.dist.php)
set -e

for FILENAME in $FILENAMES
do
	echo "Processin $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i 's/mixed\[\]/array<integer|string, null|boolean|integer|float|string|object|array>/g' "$FILENAME"
done


# Expanding 'mixed' values with scalar|object|array

echo "Processing mixed values"
set +e
FILENAMES=$(grep -r -l "mixed" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata --exclude-dir=placeholder-phpunit --exclude=.php-cs-fixer.dist.php)
set -e

for FILENAME in $FILENAMES
do
	echo "Processing $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i 's/mixed/null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array>/g' "$FILENAME"
done

