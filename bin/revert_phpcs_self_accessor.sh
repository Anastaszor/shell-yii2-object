#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR=$(dirname "$0")
CURRDIR=$(realpath "$CURRDIR/..")

REPODIR=$(realpath "$CURRDIR/..")
TODAY=$(date +%Y-%m-%d)


# Reversing the self_accessor rule by putting back the name of the class at the
# places where the self keyword was placed


echo "Processing self as first parameter"
set +e
FILENAMES=$(grep -r -l "(self" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata)
set -e

for FILENAME in $FILENAMES
do
	echo "Processing $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i "s/(self/(${BASENAME}/g" "$FILENAME" 
done


echo "Processing self as other parameter"
set +e
FILENAMES=$(grep -r -l ", self" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata)
set -e

for FILENAME in $FILENAMES
do
	echo "Processing $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i "s/, self/, ${BASENAME}/g" "$FILENAME"
done


echo "Processing self as return value"
set +e
FILENAMES=$(grep -r -l ": self" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata)
set -e

for FILENAME in $FILENAMES
do
	echo "Processing $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i "s/: self/: ${BASENAME}/g" "$FILENAME"
done


echo "Processing ?self anywhere"
set +e
FILENAMES=$(grep -r -l "\?self" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata)
set -e

for FILENAME in $FILENAMES
do
	echo "Processing $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i "s/\?self/\?${BASENAME}/g" "$FILENAME"
done


echo "Processing self as return phpdoc"
set +e
FILENAMES=$(grep -r -l "@return self" "$REPODIR" --include="*.php" --exclude-dir=vendor --exclude-dir=.metadata)
set -e

for FILENAME in $FILENAMES
do
	echo "Processing $FILENAME"
	BASENAME=$(basename "$FILENAME" '.php')
	sed -i "s/@return self/@return ${BASENAME}/g" "$FILENAME"
done


