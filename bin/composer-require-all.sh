#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR=$(dirname "$0")
CURRDIR=$(realpath "$CURRDIR/..")

REPODIR=$(realpath "$CURRDIR/..")
TODAY=$(date +%Y-%m-%d)

#
# Example usage :
# bin/composer-require-all.sh "php >=8.0"
#

if [ ! -f "$REPODIR/composer.phar" ]
then
	wget https://getcomposer.org/composer-stable.phar -O "$REPODIR/composer.phar"
fi

php "$REPODIR/composer.phar" self-update

# loop on all repositories that are found next this one
for REPO in $(find "$REPODIR" -maxdepth 1 -type d | sort -r)
do
	# ignore repositories that are not code repositories
	REPONAME=$(basename "$REPO")
	
	if [[ "$REPONAME" == $(basename "$REPODIR") ]]
	then
		continue
	fi
	
	# ignore all directories that do not start with :
	[ ${REPONAME:0:3} != "php" ] &&
	[ ${REPONAME:0:3} != "yii" ] &&
	[ ${REPONAME:0:3} != "mag" ] &&
	[ ${REPONAME:0:3} != "mtg" ] &&
	[ ${REPONAME:0:4} != "poly" ] && continue
	
	cd "$REPO"
	echo "cd $REPO"
	
	cp -v "$REPODIR/composer.phar" "$REPO/composer.phar"
	php "$REPO/composer.phar" require $@ --sort-packages
	
done
