<?php declare(strict_types=1);

/*
 * This file is part of the anastaszor/shell-yii2-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Test;

use ReflectionClass;
use Stringable;
use yii\base\BootstrapInterface;
use yii\base\Module;
use yii\BaseYii;
use yii\i18n\PhpMessageSource;

/**
 * TestModule class file.
 * 
 * This module is made to offer an api over the Testging table.
 * 
 * @author Anastaszor
 */
class TestModule extends Module implements BootstrapInterface, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\BootstrapInterface::bootstrap()
	 */
	public function bootstrap($app) : void
	{
		/** @var \yii\base\Application $app */
		$app->setAliases(['@'.\str_replace('\\', '/', __NAMESPACE__) => __DIR__]);
		if($app instanceof \yii\console\Application)
		{
			$this->controllerNamespace = __NAMESPACE__.'\\Commands';
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Module::init()
	 */
	public function init() : void
	{
		parent::init();
		
		if(null !== BaseYii::$app)
		{
			BaseYii::$app->setAliases(['@'.\str_replace('\\', '/', __NAMESPACE__) => __DIR__]);
			if(BaseYii::$app instanceof \yii\web\Application)
			{
				$this->controllerNamespace = __NAMESPACE__.'\\Controllers';
			}
			
			$className = (new ReflectionClass($this))->getShortName();
			
			BaseYii::$app->getI18n()->translations[$className.'.*'] = [
				'class' => PhpMessageSource::class,
				'sourceLanguage' => 'en-US',
				'basePath' => __DIR__.\DIRECTORY_SEPARATOR.'Messages',
			];
		}
	}
	
}
